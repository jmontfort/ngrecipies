import { ActionReducerMap } from '@ngrx/store';

import * as fromShoppingList from '../shopping-list/store/shopping-list.reducer';
import * as fromAuth from '../auth/store/auth.reducers';
import * as fromRecipes from '../recipies/store/recipe.reducers';

export interface AppState {
    shoppingList: fromShoppingList.State,
    auth: fromAuth.State,
    recipes: fromRecipes.State
}

export const reducers: ActionReducerMap<AppState> = {
    shoppingList: fromShoppingList.shoppingListReducer,
    auth: fromAuth.authReducer,
    recipes: fromRecipes.recipeReducer
};
