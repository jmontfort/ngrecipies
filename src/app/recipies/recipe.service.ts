import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

// using the old code
//import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Ingredient } from '../shared/ingredient.model';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {
    
    recipesChanged = new Subject<Recipe[]>();  

    private recepies: Recipe[] = [
    new Recipe(
      'Chicken Kathi',
      'Chicken Kathi recipe',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Chicken-kathi-roll-recipe.jpg/640px-Chicken-kathi-roll-recipe.jpg',
      [
        new Ingredient('Chicken', 1),
        new Ingredient('Katha', 1)
      ]
    ),
    new Recipe(
      'Chicken Salad',
      'Chicken Salad recipe',
      'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2010/1/21/1/FNM_030110-Weeknight-030_s4x3.jpg.rend.hgtvcom.616.462.suffix/1382539254079.jpeg',
      [
        new Ingredient('Chicken', 1),
        new Ingredient('Lettuce', 20)
      ]
    )
  ];

  constructor(
              //private shoppingListService: ShoppingListService, 
              ) { }
    
  getRecipe(index:number){
    return this.recepies[index];
  }

  getRecipes() {
    //return a new array which is an exact copy of the recipies array
    return this.recepies.slice();
  }

    // old service code
  //   addIngredientsToShoppingList(ingredients: Ingredient[]){
  //     this.shoppingListService.addIngredients(ingredients);
  // }

  addRecipe(recipe: Recipe){
    this.recepies.push(recipe);
    this.recipesChanged.next(this.recepies.slice());
  }

  setRecipies(recipies: Recipe[]) {
    this.recepies = recipies;
    this.recipesChanged.next(this.recepies.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe){
    this.recepies[index] = newRecipe;
    this.recipesChanged.next(this.recepies.slice());
  }

  deleteRecipe(index: number){
    this.recepies.splice(index, 1);
    this.recipesChanged.next(this.recepies.slice());
  }
  
}
