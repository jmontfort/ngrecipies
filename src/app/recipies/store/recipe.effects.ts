import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import 'rxjs/add/operator/switchMap';

import { Recipe } from '../recipe.model';
import * as RecipeActions from './recipe.actions';

@Injectable()
export class RecipeEffects {
    url = 'https://ng-recipies.firebaseio.com/';
    
    @Effect()
    recipeFetch = this.action$
        .ofType(RecipeActions.FETCH_RECIPES)
        .switchMap( (action: RecipeActions.FetchRecipes) => {
            return     this.httpClient.get<Recipe[]>(this.url+'recipes.json', {
                observe: 'body',
                responseType: 'json'
              }) 
        })
        .map(
            (recipes) => {
              for(let recipe of recipes)
              {
                //if the recipe does not have ingredients, add an empty array
                if(!recipe['ingredients']) {
                  recipe['ingredients'] = [];
                }
              }
              return {
                  type: RecipeActions.SET_RECIPES,
                  payload: recipes
              };
            }
            
          );

    constructor(private action$: Actions,     
                private httpClient: HttpClient) {}
}
