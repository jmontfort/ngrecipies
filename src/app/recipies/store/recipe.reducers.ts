import { Ingredient } from '../../shared/ingredient.model';
import { Recipe } from '../recipe.model';
import * as RecipeActions  from './recipe.actions';

export interface RecipeState{
    recipes: State;
}

export interface State {
    recipes: Recipe[];
}

const initialState: State = {
    recipes:[] = [
        new Recipe(
          'Chicken Kathi',
          'Chicken Kathi recipe',
          'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Chicken-kathi-roll-recipe.jpg/640px-Chicken-kathi-roll-recipe.jpg',
          [
            new Ingredient('Chicken', 1),
            new Ingredient('Katha', 1)
          ]
        ),
        new Recipe(
          'Chicken Salad',
          'Chicken Salad recipe',
          'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2010/1/21/1/FNM_030110-Weeknight-030_s4x3.jpg.rend.hgtvcom.616.462.suffix/1382539254079.jpeg',
          [
            new Ingredient('Chicken', 1),
            new Ingredient('Lettuce', 20)
          ]
        )
      ]
    };
    
export function recipeReducer(state = initialState, action: RecipeActions.RecipeActions){
  switch(action.type) {
    case (RecipeActions.SET_RECIPES): 
      return {
        ...state, 
        recipes: [...action.payload]
      };

      case (RecipeActions.ADD_RECIPE): 
        return {
          ...state, 
          recipes: [...state.recipes, action.payload]
        };
  
      case (RecipeActions.UPDATE_RECIPE):
        const recipe = state.recipes[action.payload.index];
        const updatedRecipe = {
          ...recipe,
          ...action.payload.updatedRecipe
        };
        const recipes = [...state.recipes];
        recipes[action.payload.index] = updatedRecipe;
        return {
          ...state,
          recipes: recipes
        };
  
      case (RecipeActions.DELETE_RECIPE):
        const oldRecipes = [...state.recipes];
        oldRecipes.splice(action.payload, 1);
        return {
          ...state,
          recipes: oldRecipes
        };
  
      default:
        return state;
    }

    
}