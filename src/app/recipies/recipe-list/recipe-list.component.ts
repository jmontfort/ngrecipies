import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import * as fromRecipe from '../store/recipe.reducers';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipeState: Observable<fromRecipe.State>;

  constructor(
              //old service code
              //private recipeService: RecipeService, 
              private router: Router, 
              private route: ActivatedRoute,
              private store: Store<fromRecipe.RecipeState>
            ) { }

  ngOnInit() {
    this.recipeState = this.store.select('recipes');
    //old service code
    // this.subscription = this.recipeService.recipesChanged
    //   .subscribe(
    //     (recipes: Recipe[]) => {
    //       this.recepies = recipes; 
    //     }
    //   )
    // this.recepies = this.recipeService.getRecipes()
  }

  onNewRecipe() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
