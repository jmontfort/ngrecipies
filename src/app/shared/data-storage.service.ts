import { Recipe } from '../recipies/recipe.model';
import { RecipeService } from '../recipies/recipe.service';
import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class DataStorageService {

  url = 'https://ng-recipies.firebaseio.com/';

  constructor(private httpClient: HttpClient, 
              private recipeService: RecipeService) 
              { }

  storeRecipes() {
    // return this.httpClient.put(this.url+'recipes.json', this.recipeService.getRecipes(), {
    //   observe: 'body',
    //   params: new HttpParams().set('auth', token)
    // });
    const req = new HttpRequest('PUT', this.url+'recipes.json', this.recipeService.getRecipes(), {
      reportProgress: true,
    })
    return this.httpClient.request(req);
  }

  retrieveRecipes() {
    this.httpClient.get<Recipe[]>(this.url+'recipes.json', {
      observe: 'body',
      responseType: 'json'
    }) 
      .map(
        (recipes) => {
          for(let recipe of recipes)
          {
            //if the recipe does not have ingredients, add an empty array
            if(!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        }
        
      )
      .subscribe(
        (recipes:Recipe[]) => {
          this.recipeService.setRecipies(recipes);
        }
      );    
  }
}
