import { Injectable } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';

//old service code
//import { AuthService } from '../auth/auth.service';
import * as fromApp from '../store/app.reducers';
import * as fromAuth from '../auth/store/auth.reducers';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    constructor(
        //old service code
        //private authService: AuthService
        private store: Store<fromApp.AppState>
    ){}
    
    //old service code
    // intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
    //     console.log('intercepted!', req);
    //     //const copiedReq = req.clone({headers: req.headers.set('', '')});
    //     const copiedReq = req.clone({params: req.params.set('auth', this.authService.getToken())});
    //     return next.handle(copiedReq);
    // }

    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        console.log('intercepted!', req);
        //const copiedReq = req.clone({headers: req.headers.set('', '')});
        return this.store.select('auth')
            .take(1)
            .switchMap(
                (authState: fromAuth.State) => {
                    const copiedReq = req.clone({params: req.params.set('auth', authState.token)});
                    return next.handle(copiedReq);
                }
            )
    }
}