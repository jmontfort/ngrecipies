import { RouterModule, Routes } from '@angular/router';
import { ShoppingListComponent } from './shopping-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const shoppingListRoutes: Routes = [
  {path: '', component: ShoppingListComponent},
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(shoppingListRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class ShoppingListRoutingModule { }
