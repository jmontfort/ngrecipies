import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Ingredient } from '../../shared/ingredient.model';
import * as ShoppingListActions from '../store/shopping-list.actions';
import * as fromApp from '../../store/app.reducers'

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('f') slForm: NgForm;
  editedItem: Ingredient;
  subscription: Subscription;
  editMode = false;
  //old service code
  //editedItemIndex: number;

  constructor(
              //old service code
              //private shoppingListService: ShoppingListService, 
              private store: Store<fromApp.AppState>) { }
    
  ngOnInit() {
    //old service code
    // this.shoppingListService.startedEditing
    //   .subscribe(
    //     (index: number) => {
    //       this.editedItemIndex = index;
    //       this.editMode = true;
    //       //old service code
    //       //this.editedItem = this.shoppingListService.getIngredient(index);
          
    //       this.slForm.setValue({
    //         name: this.editedItem.name,
    //         amount: this.editedItem.amount
    //       })
    //     }
    //   );

    //new store code
    this.subscription = this.store.select('shoppingList')
      .subscribe(
        (data) => {
          if(data.editedIngredientIndex > -1) {
            this.editedItem = data.editedIngredient;
            this.editMode = true;
            this.slForm.setValue({
              name: this.editedItem.name,
              amount: this.editedItem.amount
            })
          }else {
            this.editMode = false;
          }            
        }
      );
  }

  onSubmitItem(form: NgForm) {
    const value = form.value;
    const newIngredient = new Ingredient(value.name, value.amount);
    if(this.editMode) {
      //old service code
      //this.shoppingListService.updateIngredient(this.editedItemIndex, newIngredient);
      //new store code
      //this.store.dispatch(new ShoppingListActions.UpdateIngredient({index: this.editedItemIndex, ingredient: newIngredient}));
    }
    else{
      //using the service
      //this.shoppingListService.addIngredient(newIngredient);
    
      //using the store
      this.store.dispatch(new ShoppingListActions.AddIngredient(newIngredient)); 
    }
    this.editMode = false;
    this.slForm.reset();
  }

  onClearItem() {
    this.slForm.reset();
    this.editMode = false;
  }

  onDeleteItem() {
    //old service code
    //this.shoppingListService.deleteIngredient(this.editedItemIndex);
    //new store code
    //this.store.dispatch(new ShoppingListActions.DeleteIngredient(this.editedItemIndex));
    this.onClearItem();
  }

  ngOnDestroy(){
    if(this.subscription){
      this.store.dispatch(new ShoppingListActions.StopEdit());
      this.subscription.unsubscribe();
    }
  }
}
