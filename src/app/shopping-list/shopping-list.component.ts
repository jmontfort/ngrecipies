import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
//using the service
//import { Subscription } from 'rxjs/Subscription';

import { Ingredient } from '../shared/ingredient.model';
import * as ShoppingListActions from './store/shopping-list.actions';
import * as fromApp from '../store/app.reducers'


@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  // using the service
  // ingredients: Ingredient[];
  
  // using the store
  shoppingListState: Observable<{ingredients: Ingredient[]}>;
  //using the service
  //private subscription: Subscription;

  constructor(
              //old service code
              //private shoppingListService: ShoppingListService,
              private store: Store<fromApp.AppState>) { }
    
  ngOnInit() {
    // using the service
    // this.ingredients = this.shoppingListService.getIngredients();
    // this.subscription = this.shoppingListService.ingredientsChanged.subscribe(
    //   (ingredients: Ingredient[]) => {
    //     this.ingredients = ingredients;
    //   }
    // )

    // using the store
    this.shoppingListState = this.store.select('shoppingList');
  }
  
  //using the service
  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }

  onEditItem(index: number){
    //old store code
    //this.shoppingListService.startedEditing.next(index);
    this.store.dispatch(new ShoppingListActions.StartEdit(index));
  }
}
