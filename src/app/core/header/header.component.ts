import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { HttpEventType, HttpResponse } from '@angular/common/http';

import { DataStorageService } from '../../shared/data-storage.service';
import * as fromApp from '../../store/app.reducers';
import * as fromAuth from '../../auth/store/auth.reducers';
import * as AuthActions from '../../auth/store/auth.actions';
import * as RecipeActions from '../../recipies/store/recipe.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  authState: Observable<fromAuth.State>;

  constructor(private dataStorageService: DataStorageService,
              private store: Store<fromApp.AppState>
              ){}

  ngOnInit(){
    this.authState = this.store.select('auth');
  }
  onSaveData() {
    this.dataStorageService.storeRecipes()
      // .subscribe( 
      //   (response: Response) => {
      //     console.log(response);
      //   }
      // );
      .subscribe(
        (event) => {
          if(event.type == HttpEventType.UploadProgress){
            const percentDone = Math.round(100 * event.loaded / event.total);
            console.log(`File is ${percentDone}% uploaded.`);
            console.log(event);
          } else if (event.type == HttpEventType.DownloadProgress) {
            console.log(event);
          } else if (event instanceof HttpResponse) {
            console.log(event);
          }
        }
      )
    }

  onFetchData() {
    //old service code
    //this.dataStorageService.retrieveRecipes();
    this.store.dispatch(new RecipeActions.FetchRecipes());
  }

  onLogout(){
    this.store.dispatch(new AuthActions.Logout());
  }
}
