import { NgForm } from '@angular/forms/src/directives';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromApp from '../store/auth.reducers';
import * as AuthActions from '../store/auth.actions';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(
          //old service code  
          //private authService: AuthService
          private store: Store<fromApp.State>  
        ) { }

  ngOnInit() {
  }

  onSignUp(form: NgForm){
    const email = form.value.email;
    const password = form.value.password;
    //old service code
    //this.authService.signUpUser(email, password);
    this.store.dispatch(new AuthActions.Try_Signup({username: email, password: password}));
  }

}
