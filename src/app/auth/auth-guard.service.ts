import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';

import * as fromApp from '../store/app.reducers';
import * as fromAuth from '../auth/store/auth.reducers';

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(
              //old service code
              //private authService: AuthService
              private store: Store<fromApp.AppState>
            ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //old service code
    //return this.authService.isAuthenticated();
    return this.store.select('auth')
      //only take 1 instance 
      .take(1)
      .map(
        (authState: fromAuth.State) => {
          return authState.authenticated;
        } 
      );
  }
}
