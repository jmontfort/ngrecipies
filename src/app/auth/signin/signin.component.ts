import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgForm } from '@angular/forms/src/directives';

import * as fromApp from '../../store/app.reducers';
import * as AuthActions from '../store/auth.actions';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(
              //old service code
              //private authService: AuthService
              private store: Store<fromApp.AppState>
            ) { }

  ngOnInit() {
  }

  onSignIn(form: NgForm){
    //old service code
    //this.authService.signInUser(form.value.email, form.value.password);
    this.store.dispatch(new AuthActions.Try_Signin({username: form.value.email, password: form.value.password}));
  }

}
