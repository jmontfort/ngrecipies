import { NgRecipiesPage } from './app.po';

describe('ng-recipies App', () => {
  let page: NgRecipiesPage;

  beforeEach(() => {
    page = new NgRecipiesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
